from time import sleep
import board
from adafruit_bme280 import basic as adafruit_bme280
import paho.mqtt.client as mqtt
import socket

i2c = board.I2C()  # uses board.SCL and board.SDA
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)
bme280.sea_level_pressure = 1013.25

brokerIP = "10.56.18.5"
brokerPort = 1883
brokerKeepAlive = 60

myTopic1 = "BMP/Temp"
myTopic2 = "BMP/Humidity"
myTopic3 = "BMP/Pressure"


myQoS = 0
myRetain = True
client = mqtt.Client()
client.connect(brokerIP,brokerPort,brokerKeepAlive)
while True:
    Payload1 = round(bme280.temperature,2)
    Payload2 = round(bme280.relative_humidity,2)
    Payload3 = round(bme280.pressure,2)
    client.publish(topic = myTopic1, qos = myQoS, payload = Payload1, retain = myRetain)
    client.publish(topic = myTopic2, qos = myQoS, payload = Payload2, retain = myRetain)
    client.publish(topic = myTopic3, qos = myQoS, payload = Payload3, retain = myRetain)
    sleep(2)


"""
while True:
    print("\nTemperature: %0.1f C" % bme280.temperature)
    print("Humidity: %0.1f %%" % bme280.relative_humidity)
    print("Pressure: %0.1f hPa" % bme280.pressure)
    print("Altitude = %0.2f meters" % bme280.altitude)
    time.sleep(2)
"""